package org.benoitharrault.ameixanonfree.interfaces;

import android.graphics.Bitmap;

public interface BitmapListener {
    void onBitmap(Bitmap bitmap);
}
