#!/bin/bash

# Check optipng dependency
command -v optipng >/dev/null 2>&1 || { echo >&2 "I require optipng but it's not installed. Aborting."; exit 1; }

# Filter icon name to speed up optimize process
FILTER_ICON_NAME="$1"

ICON_TYPES="chromatic"
ICON_SIZES="drawable-mdpi drawable-hdpi drawable-xhdpi drawable-xxhdpi drawable-xxxhdpi"

TYPES_COUNT="$(echo "${ICON_TYPES}" | wc -w)"
SIZES_COUNT="$(echo "${ICON_SIZES}" | wc -w)"

OPTIPNG_OPTIONS="-preserve -quiet -o7"

TYPE_INDEX=0
for TYPE in ${ICON_TYPES}
do
    TYPE_INDEX=$((TYPE_INDEX+1))
    PROGRESS="(TYPE: ${TYPE_INDEX}/${TYPES_COUNT})"

    echo ""
    echo "Type: ${TYPE} ${PROGRESS}"

    SIZE_INDEX=0
    for SIZE in ${ICON_SIZES}
    do
        SIZE_INDEX=$((SIZE_INDEX+1))
        PROGRESS="(TYPE: ${TYPE_INDEX}/${TYPES_COUNT} - SIZE: ${SIZE_INDEX}/${SIZES_COUNT})"

        echo ""
        echo "Size: ${SIZE} ${PROGRESS}"

        BASE_FOLDER="app/src/${TYPE}/res/${SIZE}"
        FILES_COUNT="$(ls ${BASE_FOLDER} | grep "\.png$" | wc -l)"
        FILE_INDEX=0

        for FILE in ${BASE_FOLDER}/*.png
        do
            FILE_INDEX=$((FILE_INDEX+1))
            PROGRESS="(TYPE: ${TYPE_INDEX}/${TYPES_COUNT} - SIZE: ${SIZE_INDEX}/${SIZES_COUNT} - FILE: ${FILE_INDEX}/${FILES_COUNT})"

            if [[ ! -z "${FILTER_ICON_NAME}" ]] && [[ "${BASE_FOLDER}/${FILTER_ICON_NAME}.png" != "${FILE}" ]]; then
                continue
            fi

            echo "Optimizing file: ${PROGRESS} ${FILE}"
            if [[ -f "${FILE}" ]]
            then
                optipng ${OPTIPNG_OPTIONS} ${FILE}
            else
                echo "Warning: File not found: ${FILE}"
            fi
        done
    done
done
