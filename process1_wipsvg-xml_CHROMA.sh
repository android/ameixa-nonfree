#! /bin/bash

# Check dependencies
command -v inkscape >/dev/null 2>&1 || { echo >&2 "I require inkscape but it's not installed. Aborting."; exit 1; }
command -v scour >/dev/null 2>&1 || { echo >&2 "I require scour but it's not installed. Aborting."; exit 1; }

ICON_OPTIONS="
    mdpi:48
    hdpi:72
    xhdpi:96
    xxhdpi:144
    xxxhdpi:192
"

# get from todo
FILES_COUNT="$(ls todo | grep "\.svg$" | wc -l)"
FILE_INDEX=0
for SVG in todo/*.svg
do
    FILE_INDEX=$((FILE_INDEX+1))
    PROGRESS="(FILE: ${FILE_INDEX}/${FILES_COUNT})"

    if [[ -f "${SVG}" ]]; then
        ICON_NAME=$(basename ${SVG} .svg)
        echo "Building icon: ${PROGRESS} ${ICON_NAME}"

        # clean/optimize SVG file
        cp ${SVG} ${SVG}.tmp
        scour \
            --remove-descriptive-elements \
            --enable-id-stripping \
            --enable-viewboxing \
            --enable-comment-stripping \
            --nindent=4 \
            --quiet \
            -i ${SVG}.tmp \
            -o ${SVG}
        rm ${SVG}.tmp

        # build icon variants
        for ICON_OPTION in ${ICON_OPTIONS}
        do
            ICON_TYPE="${ICON_OPTION%%:*}"
            ICON_SIZE="${ICON_OPTION##*:}"
            inkscape \
                --export-width=${ICON_SIZE} \
                --export-height=${ICON_SIZE} \
                --export-filename=app/src/chromatic/res/drawable-${ICON_TYPE}/${ICON_NAME}.png \
                ${SVG}
        done

        mv ${SVG} icons/chromatic
    fi
done

# "xml" create corresponding "values/iconpack.xml" and "xml/drawable.xml"
SVGDIR="icons/chromatic/"
EXPORT="app/src/main/res"
ICPACK_PRE='        <item>'
ICPACK_SUF='</item>\n'
DRAWABLE_PRE='    <item drawable="'
DRAWABLE_SUF='" />\n'

printf '<?xml version="1.0" encoding="utf-8"?>\n<resources>\n    <string-array name="icon_pack" translatable="false">\n' > iconpack.xml
printf '<?xml version="1.0" encoding="utf-8"?>\n<resources>\n    <version>1</version>\n' > drawable.xml

for DIR in $(find ${SVGDIR} -name "*.svg" | sed "s/\.svg$//g" | sort)
do
    FILE=${DIR##*/}
    NAME=${FILE%.*}
    printf "${ICPACK_PRE}${NAME}${ICPACK_SUF}" >> iconpack.xml
    printf "${DRAWABLE_PRE}${NAME}${DRAWABLE_SUF}" >> drawable.xml
done

printf '    </string-array>\n</resources>\n' >> iconpack.xml
printf '</resources>\n' >> drawable.xml

mv -f iconpack.xml ${EXPORT}/values/
mv -f drawable.xml ${EXPORT}/xml/
