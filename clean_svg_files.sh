#!/bin/bash

SVG_BASE_CHROMATIC_FOLDER="icons/chromatic"

function remove_svg_string() {
    SVG_STRING_TO_REMOVE="$1"

    echo "Will remove ${SVG_STRING_TO_REMOVE}"

    for SVG_FILE in ${SVG_BASE_CHROMATIC_FOLDER}/*.svg
    do
        sed -i "s|${SVG_STRING_TO_REMOVE}||g" "${SVG_FILE}"
    done
}

# Patterns found in <g display="none"> blocks
remove_svg_string '<flowPara font-size="4px" style="line-height:1.25">your</flowPara>'
remove_svg_string '<flowPara/>'
remove_svg_string '<flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" stroke-width="1.1378px"/></flowRegion>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="12.8px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" stroke-width="1.1378px"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="12.8px" stroke-width="1.0667px" style="line-height:0.01%" xml:space="preserve"></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke="#fd0000" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" stroke="#fd0000"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#000000"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#000"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="3.75px" stroke-width="1px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="3.75px" stroke-width=".91241px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" stroke-width=".91241px"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="3.75px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" font-size="4px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-family="Roboto" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara font-size="3.75px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-size="3.75px" stroke-width="1px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#000000" font-size="3.75px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#c981ea" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#c981ea"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#c981ea"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffff00" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#ffff00"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#ff0"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="12.8px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" fill="#fff"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="12.8px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" fill="#fff" stroke-width="1.1378px"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size=".2334px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#ffffff"><rect x="658.87" y="447.68" width=".27946" height=".31439" fill="#fff"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1.0096px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#fff" stroke-width="1.0096px"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1.997px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#fff" stroke-width="1.997px"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#ffffff"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#fff"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#fff"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot fill="#ffffff" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width=".95348px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#fff" stroke-width=".95348px"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot font-family="Roboto" font-size="12.8px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" fill="#fff" stroke-width="1.1378px"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot font-family="Roboto" font-size="12.8px" stroke-width="1.0667px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" stroke-width="1.1378px"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="matrix(.041503 0 0 .041503 61.256 14.174)" fill="#ffffff" font-family="Roboto" font-size="12.8px" letter-spacing="0px" stroke-width="1.0667px" word-spacing="0px" style="line-height:0.01%" xml:space="preserve"><flowRegion><rect x="11.375" y="11.984" width="4.7893" height="5.388" fill="#fff" stroke-width="1.1378px"/></flowRegion><flowPara font-size="4px" style="line-height:1.25">your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="matrix(1.1131 0 0 1.1131 -.9146 -2.1642)" fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="matrix(.58635 0 0 .58635 107 -118.39)" fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="matrix(.60497 0 0 .60497 10.057 10.173)" fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="matrix(.9791 0 0 .9791 .15252 .41427)" fill="#000000" font-family="Roboto" font-size="1.2178px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="-.28564" y="25.935" width="1.4581" height="1.6404"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="translate(165 -127)" fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="translate(-22.312 3.8052)" fill="#008000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#008000"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#008000"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="translate(-57.463 -1.975)" fill="#008000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion fill="#008000"><rect x="10.664" y="11.235" width="4.49" height="5.0512" fill="#008000"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<flowRoot transform="translate(-90 22.5)" fill="#000000" font-family="Roboto" font-size="3.75px" letter-spacing="0px" stroke-width="1px" word-spacing="0px" style="line-height:125%" xml:space="preserve"><flowRegion><rect x="10.664" y="11.235" width="4.49" height="5.0512"/></flowRegion><flowPara>your</flowPara></flowRoot>'
remove_svg_string '<path d="m6.7726 8.112h13.405v10.726h-13.405z" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06524"/>'
remove_svg_string '<path transform="rotate(90)" d="m6.7726-18.838h13.405v10.726h-13.405z" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06524"/>'
remove_svg_string '<path transform="rotate(90)" d="m7.3342-19.616h12.282v12.282h-12.282z" display="inline" stroke="#000" stroke-width=".065984"/>'
remove_svg_string '<rect transform="rotate(90)" x="6.7726" y="-18.838" width="13.405" height="10.726" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".0068994"/>'
remove_svg_string '<rect transform="rotate(90)" x="6.7726" y="-18.838" width="13.405" height="10.726" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06524"/>'
remove_svg_string '<rect transform="rotate(90)" x="6.7726" y="-18.838" width="13.405" height="10.726" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".11647"/>'
remove_svg_string '<rect transform="rotate(90)" x="6.7726" y="-18.838" width="13.405" height="10.726" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".13029"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.2241" y="-20.094" width="14.298" height="11.441" display="inline" opacity="1" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06959"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.2241" y="-20.094" width="14.298" height="11.441" display="inline" stroke="#0f0"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.2241" y="-20.094" width="14.298" height="11.441" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".069589"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.2241" y="-20.094" width="14.298" height="11.441" display="inline" stroke="#0f0" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06959"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.3342" y="-19.616" width="12.282" height="12.282" display="inline" stroke="#000" stroke-width=".006978"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.3342" y="-19.616" width="12.282" height="12.282" display="inline" stroke="#000" stroke-width=".065984"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.3342" y="-19.616" width="12.282" height="12.282" display="inline" stroke="#000" stroke-width=".11779"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.3342" y="-19.616" width="12.282" height="12.282" display="inline" stroke="#000" stroke-width=".13177"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.8232" y="-20.923" width="13.1" height="13.1" display="inline" opacity="1" stroke="#000" stroke-width=".070382"/>'
remove_svg_string '<rect transform="rotate(90)" x="7.8232" y="-20.923" width="13.1" height="13.1" display="inline" stroke="#000" stroke-width=".070382"/>'
remove_svg_string '<rect x="6.7726" y="8.112" width="13.405" height="10.726" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".0068994"/>'
remove_svg_string '<rect x="6.7726" y="8.112" width="13.405" height="10.726" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06524"/>'
remove_svg_string '<rect x="6.7726" y="8.112" width="13.405" height="10.726" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".11647"/>'
remove_svg_string '<rect x="6.7726" y="8.112" width="13.405" height="10.726" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".13029"/>'
remove_svg_string '<rect x="7.2241" y="8.6528" width="14.298" height="11.441" opacity="1" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06959"/>'
remove_svg_string '<rect x="7.2241" y="8.6528" width="14.298" height="11.441" stroke="#5261ff"/>'
remove_svg_string '<rect x="7.2241" y="8.6528" width="14.298" height="11.441" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".069589"/>'
remove_svg_string '<rect x="7.2241" y="8.6528" width="14.298" height="11.441" stroke="#5261ff" stroke-linecap="round" stroke-linejoin="round" stroke-width=".06959"/>'

echo "Ok, done. You should rebuild all icons."
